# JWT Tokens PowerShell

A simple PowerShell module for encoding, decoding and debugging [JSON Web Tokens](http://jwt.io)
