﻿// The MIT License (MIT)
// 
// Copyright (C) 2015, Matthew Kleinschafer.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace JwtTokens.PowerShell
{
    using System.Management.Automation;
    using JWT;

    /// <summary>
    /// Decodes an existing JWT token
    /// </summary>
    [Cmdlet("Decode", "Jwt")]
    public class DecodeJwt : Cmdlet
    {
        [Parameter(Mandatory = true, HelpMessage = "The shared secret with which to decode the token")]
        public string Secret { get; set; }

        [Parameter(Mandatory = true, Position = 0, ValueFromPipeline = true, HelpMessage = "The token to debug")]
        public string[] Token { get; set; }

        [Parameter(Mandatory = false, HelpMessage = "True to verify the resultant token. True by default")]
        public bool Verify { get; set; } = true;

        protected override void ProcessRecord()
        {
            foreach (var token in Token)
            {
                var decoded = JsonWebToken.Decode(token, Secret, Verify);

                WriteObject(decoded);
            }
        }
    }
}
