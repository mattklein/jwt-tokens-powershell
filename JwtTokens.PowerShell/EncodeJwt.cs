﻿// The MIT License (MIT)
// 
// Copyright (C) 2015, Matthew Kleinschafer.
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

namespace JwtTokens.PowerShell
{
    using System.Collections;
    using System.Management.Automation;
    using System.Text;
    using JWT;

    /// <summary>
    /// Encodes a new JWT token.
    /// </summary>
    [Cmdlet("Encode", "Jwt")]
    public class EncodeJwt : Cmdlet
    {
        [Parameter(Mandatory = true, HelpMessage = "The shared secret with which to sign the token")]
        public string Secret { get; set; }

        [Parameter(Mandatory = true, Position = 0, HelpMessage = "The claims to embed into the token")]
        public Hashtable Claims { get; set; }

        [Parameter(Mandatory = false, HelpMessage = "The hash algorithm to use. HS256 by default")]
        public JwtHashAlgorithm HashAlgorithm { get; set; } = JwtHashAlgorithm.HS256;

        protected override void ProcessRecord()
        {
            var secretBytes = Encoding.UTF8.GetBytes(Secret);
            var encoded = JsonWebToken.Encode(Claims, secretBytes, HashAlgorithm);

            WriteObject(encoded);
        }
    }
}
